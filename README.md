# acc-models-lhc Python Package

## Getting started

```
pip install acc-models-lhc
```

```python
from acc-models-lhc import LHC
```

## Structure

LHC  : Machine
LHC.run2023 : Scenario
LHC.run2023.pp_lumi : Cycle
LHC.run2023.pp_lumi.ramp : Process
LHC.run2023.pp_lumi.ramp(time=0) : OpticsModel

LHC.hl16.round: OpticsSet
LHC.hl16.round(betxir5=0.15) : OpticsModel

OpticsModel: twiss, aperture, plot, rematch, etc...

